resource "proxmox_vm_qemu" "k8s_masters" {
  vmid         = "${count.index + var.vmid_begin_masters}"
  target_node  = var.target_node
  name         = var.master_name[count.index]
  pool         = var.resource_pool
  
  agent = var.agent
  os_type = var.os_type
  clone = var.template_name
  
  scsihw = var.scsihw
  bootdisk = var.bootdisk
  cores  = var.cores
  sockets = var.sockets
  cpu = var.cpu
  memory = var.memory
  
  disk {
    storage = var.disk.storage
    size    = var.disk.size
    type    = var.disk.type
    iothread = var.disk.iothread
  } 

  network {
    model = var.network.model
    bridge = var.network.bridge
    tag    = var.network.tag
  }

  ipconfig0 = "ip=${var.master_pool_ip[count.index]}/${var.mask},gw=${var.gateway}"

  serial {
    id = var.serial.id
    type = var.serial.type
  }

  sshkeys = var.sshkeys
  onboot  = var.onboot

  count = var.machine_counts_masters
}

resource "proxmox_vm_qemu" "k8s_workers" {
  vmid         = "${count.index + var.vmid_begin_workers}"
  target_node  = var.target_node
  name         = var.worker_name[count.index]
  pool         = var.resource_pool
  
  agent = var.agent
  os_type = var.os_type
  clone = var.template_name
  
  scsihw = var.scsihw
  bootdisk = var.bootdisk
  cores  = var.cores
  sockets = var.sockets
  cpu = var.cpu
  memory = var.memory
  
  disk {
    storage = var.disk.storage
    size    = var.disk.size
    type    = var.disk.type
    iothread = var.disk.iothread
  } 

  network {
    model = var.network.model
    bridge = var.network.bridge
    tag    = var.network.tag
  }

  ipconfig0 = "ip=${var.worker_pool_ip[count.index]}/${var.mask},gw=${var.gateway}"

  serial {
    id = var.serial.id
    type = var.serial.type
  }

  sshkeys = var.sshkeys
  onboot  = var.onboot

  count = var.machine_counts_workers
}

resource "local_file" "hosts_cfg" {
  content  = templatefile("ansible-inventory.tpl",
    {
      clients_ip_master = proxmox_vm_qemu.k8s_masters.*.ipconfig0
      clients_ip_worker = proxmox_vm_qemu.k8s_workers.*.ipconfig0
      clients_name_master = proxmox_vm_qemu.k8s_masters.*.name
      clients_name_worker = proxmox_vm_qemu.k8s_workers.*.name
    }
  )
  filename = "./hosts-non-cleaned.cfg"
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = <<-EOT
          sed 's/ip=/ansible_host=/g' hosts-non-cleaned.cfg > hosts1.cfg
          sed 's/\/24,gw=192.168.127.1//g' hosts1.cfg > inventory.txt
          rm hosts1.cfg
EOT
  }
  depends_on = [ local_file.hosts_cfg ]
}