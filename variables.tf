variable "proxmox_api_url" {
  type        = string
  description = "URL proxmox"
}
variable "proxmox_api_token_id" {
  type        = string
  description = "TOKEN ID"
  sensitive = true
}
variable "proxmox_api_token_secret" {
  type        = string
  description = "SECRET TOKEN"
  sensitive = true
}
variable "vmid_begin_masters" {
  type        = number
  description = "VM ID machine begin"
  default = 510
}

variable "vmid_begin_workers" {
  type        = number
  description = "VM ID machine begin"
  default = 530
}

variable "target_node" {
  type        = string
  description = "A string containing the cluster node name"
}
variable "master_name" {
  type        = list(string)
  description = "Specifies the host master name of the VM."
}

variable "worker_name" {
  type        = list(string)
  description = "Specifies the host worker name of the VM."
}

variable "os_type" {
  type        = string
  description = "The volume identifier that points to the OS template or backup file."
  default = "cloud-init"
}

variable "template_name" {
  type = string
  default = "kubmaster-template"
}

variable "agent" {
  type        = number
  description = "agent"
}

variable "bootdisk" {
  type        = string
  description = "bootdisk"
}

variable "sockets" {
  type        = number
  description = "sockets"
}

variable "cpu" {
  type        = string
  description = "cpu"
}

variable "scsihw" {
  type        = string
  description = "scsihw"
}

variable "cores" {
  type        = number
  description = "The number of cores assigned to the VM"
}
variable "memory" {
  type        = string
  description = "A number containing the amount of RAM to assign to the VM (in MB)."
}

variable "resource_pool" {
  type        = string
  description = "Name of resource pool to create virtual machine in."
  default     = "vlad"
}

variable "disk" {
  type = object({
    storage = string
    size    = string
    type    = string
    iothread = number
  })
  description = "An object for configuring the root mount point of the VM. Can only be specified once."
}

variable "network" {
  type = object({
    model   = string
    bridge = string
    tag    = string
  })

  description = "An object defining a network interface for the VM"
}

variable "gateway" {
  type = string
  description = "gateway"
}

variable "mask" {
  type = string
  description = "Mask"
}

variable "master_pool_ip" {
  type = list(string)
  description = "An object defining a master_pool_ip interface for the VM"
}

variable "worker_pool_ip" {
  type = list(string)
  description = "An object defining a worker_pool_ip interface for the VM"
}

variable "serial" {
  type = object({
    id   = number
    type = string
  })
}

variable "sshkeys" {
  type        = string
  description = "Multi-line string of SSH public keys that will be added to the VM"
}
variable "onboot" {
  default     = true
  type        = bool
  description = "A boolean that determines if the VM is started after creation"
}

variable "machine_counts_masters" {
  default     = 1
  type        = number
  description = "Number of machines that must be create"
}

variable "machine_counts_workers" {
  default     = 1
  type        = number
  description = "Number of machines that must be create"
}