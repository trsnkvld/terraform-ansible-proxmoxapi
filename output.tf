output "ip_adresses_pool_master" {
    value = var.master_pool_ip
}

output "ip_adresses_pool_worker" {
    value = var.worker_pool_ip
}