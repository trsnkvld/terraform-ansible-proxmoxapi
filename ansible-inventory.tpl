[k8master]
%{ for ipm in clients_ip_master ~}
%{ for nm in clients_name_master ~}
${nm} ${ipm}
%{ endfor ~}
%{ endfor ~}

[k8worker]
%{ for ipw in clients_ip_worker ~}
%{ for nw in clients_name_worker ~}
${nw} ${ipw}
%{ endfor ~}
%{ endfor ~}

[k8cluster:children]
k8master
k8worker
